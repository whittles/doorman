<h3>About</h3>
A simple Python script to monitor (in my case) a door. The sensor is a simple magnetic reed switch that opens and closes a circuit. When the state changes it triggers an IFTTT event.