import time
import RPi.GPIO as GPIO
import urllib.request
import yaml
import os

# nano /etc/rc.local and uncomment line to make run on boot

def importSettings():
	dir_path = os.path.dirname(os.path.realpath(__file__))
	with open(dir_path + '/settings.yaml', 'r') as f:
		doc = yaml.load(f)
	return doc

def send_event(EVENT):
	try:
		response = urllib.request.urlopen(BASE_URL + EVENT + '/with/key/' + KEY)
		print(response.read())
	except Exception as e:
		print(e)

def led_status(color):
	if color == 'red':
		GPIO.output(RED_PIN, GPIO.HIGH)
		GPIO.output(GREEN_PIN, GPIO.LOW)
		GPIO.output(BLUE_PIN, GPIO.LOW)
	elif color == 'green':
		GPIO.output(RED_PIN, GPIO.LOW)
		GPIO.output(GREEN_PIN, GPIO.HIGH)
		GPIO.output(BLUE_PIN, GPIO.LOW)
	elif color == 'blue':
		GPIO.output(RED_PIN, GPIO.LOW)
		GPIO.output(GREEN_PIN, GPIO.LOW)
		GPIO.output(BLUE_PIN, GPIO.HIGH)
	elif color == 'off':
		GPIO.output(RED_PIN, GPIO.LOW)
		GPIO.output(GREEN_PIN, GPIO.LOW)
		GPIO.output(BLUE_PIN, GPIO.LOW)

def checkStatus(status):
	if status == 0:
		led_status('green')
	elif status == 1:
		led_status('red')
	if DEBUG:
		print('Status:', STATUS)

def initStatus():
	STATUS = GPIO.input(SENSOR_PIN)
	if DEBUG:
		print('Initialized. Status:', STATUS)

settings = importSettings()

SENSOR_PIN = settings['pins']['sensor']
BASE_URL = settings['ifttt']['url']
KEY = settings['ifttt']['key']
STATUS = settings['gpio']['default_sensor_status']

DEBUG = settings['misc']['debug_mode']

RED_PIN = settings['pins']['rgb_red']
GREEN_PIN = settings['pins']['rgb_green']
BLUE_PIN = settings['pins']['rgb_blue']

# Configure the GPIO pin
gpio_mode = str.upper(settings['gpio']['mode'])
if gpio_mode == 'BCM':
    GPIO.setmode(GPIO.BCM)
elif gpio_mode == 'BOARD':
    GPIO.setmode(GPIO.BOARD)

gpio_updown = str.upper(settings['gpio']['sensor_updown'])
if gpio_updown == 'UP':
    GPIO.setup(SENSOR_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)
elif gpio_updown == 'DOWN':
    GPIO.setup(SENSOR_PIN, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)


GPIO.setup(RED_PIN, GPIO.OUT)
GPIO.setup(GREEN_PIN, GPIO.OUT)
GPIO.setup(BLUE_PIN, GPIO.OUT)


initStatus()		
checkStatus(STATUS)

try:
    while True:
        if GPIO.input(SENSOR_PIN) == 1 and STATUS == 0:
            	# door open
            send_event('NARNIA_OPEN')
            STATUS = 1
		
        elif GPIO.input(SENSOR_PIN) == 0 and STATUS == 1:
            # door closed
            send_event('NARNIA_CLOSED')
            STATUS = 0
        checkStatus(STATUS)
        time.sleep(0.5)

finally:
	print('Cleaning up GPIO')
	led_status('off')
	GPIO.cleanup()
